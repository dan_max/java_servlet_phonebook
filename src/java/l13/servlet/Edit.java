/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l13.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import l13.model.*;
/**
 *
 * @author danmax
 */
public class Edit extends HttpServlet {
    private String res = "";
    private UserDB db = new UserDB();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            db.load();
            HashSet<User> data = db.getData();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
                out.println("<title>Edit</title>");            
            out.println("</head>");
            out.println("<body>");
                out.println("<form autocomplete=\"on\">");
                    out.println("<table>");
                        out.println("<tr>");
                            out.println("<td> Имя </td>");
                            out.println("<td> <p><input name=\"user\"></p> </td>");
                        out.println("</tr>");
                    
                        out.println("<tr>");
                            out.println("<td> Телефон </td>");
                            out.println("<td> <p><input name=\"phone\"></p> </td>");
                        out.println("</tr>");
                    
                        out.println("<tr>");
                            out.println("<td colspan=\"2\">"+ res +"</td>>");
                        out.println("</tr>");
                        
                        out.println("<tr>");
                            out.println("<td colspan=\"2\"> <input type=\"submit\" value=\"Отправить\"> </td>>");
                        out.println("</tr>");
                    out.println("</table>");
                out.println("</form>");
            out.println("</body>");
            out.println("</html>");
            res = "";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("user");
        Long phone;
        boolean finded = false;
        if(request.getParameter("phone") != null && name != null){
            phone = Long.parseLong(request.getParameter("phone"));
            HashSet<User> data = db.getData();
            for(User i : data){
                if(i.getName().equals(name)){
                    finded = true;
                    if(i.addPhone(phone)){
                        res = "Добавленно";
                    }
                    else{
                        res = "Номер уже есть";
                    }
                }
            }
            if(!finded){
                User u = new User(name, phone);
                db.add(u);
                res = "Добавленно";
            }
            db.save();
        }
        else{
            res = "empty phone or name";
        }
        
        
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
