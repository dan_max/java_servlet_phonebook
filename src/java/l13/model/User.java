
package l13.model;

import java.util.HashSet;
/**
 *
 * @author danmax
 */
public class User {
    private String name;
    private final HashSet<Long> phones = new HashSet<Long>(); 
    public User(){}
    public User(String n){
        name = n;
    }
    public User(String n, Long phone){
        name = n;
        phones.add(phone);
    }
    public boolean addPhone(long phone){
        return phones.add(phone);
    }
    public void removePhone(long phone){
        if(!phones.remove(phone)){
            //iskluxenie
        }
    }
    public HashSet<Long> getPhones(){
        return phones;
    }
    public String getName(){
        return name;
    }
    public void setName(String n){
        name = n;
    }
    @Override
    public boolean equals(Object o) {
    if (!(o instanceof User)) return false;
    User uo = (User) o;
    if(!this.name.equals(uo.getName())){
        return false;
    }
    HashSet<Long> ophones = uo.getPhones();
    if(ophones.equals(phones)){
        return true;
    }
    else{
        return false;
    }
  }

}
