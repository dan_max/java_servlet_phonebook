package l13.model;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;

public class UserDB {
    private final HashSet<User> data = new HashSet<User>();
    
    public UserDB(){}
    
    public UserDB(User u){
        data.add(u);
    }
    
    public void add(User u){
        data.add(u);
    }
    
    public UserDB(User[] u, int size){
        for(int i = 0; i < size; ++i){
            if(!data.add(u[i])){
                //isk;
            }
        }
    }
    
    
    public void save() throws IOException{
        try(FileWriter writer = new FileWriter("DB11.txt", false)){
            writer.write(data.size());
            writer.append('\n');
            for(User i : data){
                writer.write(i.getName());
                writer.append('\n');
                writer.write(i.getPhones().size());
                writer.append('\n');
                for(long j : i.getPhones()){
                    writer.write((int) j);
                    writer.append('\n');
                }
            }
            writer.flush();
            writer.close();
        }
    }
    
    public void load() throws IOException{
        try(FileReader reader = new FileReader("c:\\Users\\danmax\\source\\repos\\java_servlet_phonebook\\src\\java\\l13\\model\\DB.txt"))
        {
            data.clear();
            Scanner scan = new Scanner(reader);
            int user_count = Integer.parseInt(scan.nextLine());
            int phone_count;
            for(int i = 0; i < user_count; ++i){
                User u = new User(scan.nextLine());
                phone_count = Integer.parseInt(scan.nextLine());
                for(int j = 0; j < phone_count; ++j){
                    Long phone = Long.parseLong(scan.nextLine());
                    u.addPhone(phone);
                }
                data.add(u);
            }
            reader.close();
        }
    }
    
    public HashSet<User> getData(){
        return data;
    }
    
    public void remove(User u) throws IOException{
        if(!data.remove(u)){
            throw new IOException("DB alredy dosn`t have this user");
        }
    }
}
